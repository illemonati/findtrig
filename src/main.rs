
use rayon::prelude::*;


const TRANSFORMATION_FUNCTIONS: [fn(f64) -> f64; 6] = [f64::sin, f64::cos, f64::tan, f64::asin, f64::acos, f64::atan];
const TRNASFORMATION_NAMES: [&'static str; 6] = ["sin", "cos", "tan", "asin", "acos", "atan"];


#[derive(Debug, Clone)]
struct FindSequenceResult {
    steps: Vec<usize>,
    found: bool
}


fn translate_steps(steps: &Vec<usize>) -> String {
    let mut res = String::from("0");
    for step in steps {
        res = format!("{}({})", TRNASFORMATION_NAMES[*step], res);
    }
    res
}


fn findsequence(number_to_find: f64, number: f64, steps: Vec<usize>) {
    if f64::abs(number - number_to_find) < 0.0000001 {
        println!("{} - {:?} - steps translated: {}", number, steps, translate_steps(&steps));
    }

    if steps.len() > 20 {
        return;
    }

    (0usize..5).into_par_iter().for_each(|i| {
        let transformation: fn(f64) -> f64 = TRANSFORMATION_FUNCTIONS[i];
        let new_number = transformation(number);
        let mut new_steps = steps.clone();
        new_steps.push(i);
        findsequence(number_to_find, new_number, new_steps)
    });
}


fn main() {
    let number = 3f64/f64::sqrt(5.0);
    println!("finding {}", number);
    findsequence(number, 0f64, vec![])
}


